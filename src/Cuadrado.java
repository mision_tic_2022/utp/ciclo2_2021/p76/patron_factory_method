public class Cuadrado extends Figura {
    private double lado;

    public Cuadrado(double lado){
        this.lado = lado;
    }

    public double getPerimetro(){
        return 4 * this.lado;
    }

    public double getArea(){
        return 2 * this.lado;
    }

}
