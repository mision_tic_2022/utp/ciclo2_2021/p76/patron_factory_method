public class FabricaFiguras{

    public static Figura crear_cuadrado(double lado){
        return new Cuadrado(lado);
    }

    public static Figura crear_circulo(double radio){
        return new Circulo(radio);
    }

}