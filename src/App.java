public class App {
    public static void main(String[] args) throws Exception {
        
        Figura obj_cuadrado = FabricaFiguras.crear_cuadrado(10);

        System.out.println("Perimetro cuadrado: "+obj_cuadrado.getPerimetro());

        Figura obj_circulo = FabricaFiguras.crear_circulo(8);

        System.out.println("Perimetro circulo: "+obj_circulo.getPerimetro());

    }
}
